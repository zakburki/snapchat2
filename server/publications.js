Meteor.publish("selfies", function (userId) {
  if(userId){
  var from = Meteor.users.findOne({_id:userId}).emails[0].address;
  return Snaps.find({from: from});
  }
});

Meteor.publish("snaps", function (userId) {
  if(userId){
    var to = Meteor.users.findOne({_id:userId}).emails[0].address;
    return Snaps.find({to: to});
  }
});

Meteor.publish("snap-by-id", function (id) {
  return Snaps.find({_id: id});
});
