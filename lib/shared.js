Snaps = new Meteor.Collection("snaps");

Router.route('home', {path: '/'});

Router.route('list', {path: '/list/:userId',
    waitOn: function(){
      Meteor.subscribe("snaps", this.params.userId);
    }
});

Router.route('snap', {path: '/snap/:id',
    waitOn: function () {
      Meteor.subscribe("snap-by-id", this.params.id);
    },
    data: function () {
      return Snaps.findOne(this.params.id);
    },
    onAfterAction: function(){
      var snap = Snaps.findOne(this.params.id);
      if(snap){
        Meteor.setTimeout(function(){
          Snaps.remove(snap._id);
          Router.go('list',{userId: Meteor.user()._id});
        },snap.time*1000);
      }
    }
  }
);
